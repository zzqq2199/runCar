import RPi.GPIO as GPIO
from ctypes import cdll


class Remote():
    def __init__(self):
        import RPi.GPIO as GPIO
        from ctypes import cdll
        self.c = dict()
        self.p= dict()
        self.speed =50
        self.direction = 'w'
        self.ws = 'w'
        self.ad = 'p' #neither a nor d

        self.c['power+'] = self.c['power']=10
        self.c['power-'] = self.c['ground'] = 6
        self.c['le'] = self.c['leftEnable'] = 3
        self.c['l1'] = self.c['l+'] = 5
        self.c['l2'] = self.c['l-'] = 7
        self.c['re'] = self.c['rightEnable']=8
        self.c['r1'] = self.c['r+'] = 31
        self.c['r2'] = self.c['r-'] = 29

        self.inputLib = cdll.LoadLibrary("getInput/getInput.so")

        self.tbl = {
            'w':{'w':'w','s':'s','a':'w','d':'w'},
            's':{'w':'w','s':'s','a':'s','d':'s'},
            'a':{'w':'p','s':'p','a':'a','d':'d'},
            'd':{'w':'p','s':'p','a':'a','d':'d'},
            'p':{'w':'p','s':'p','a':'a','d':'d'}
        }

        self.funList = dict()
        self.funList['g'] = self.go
        self.funList[' '] = self.stop
        self.funList['p'] = self.pause
        self.funList['q'] = self.quit
        for key in ['w','s','a','d']:
            self.funList[key] = lambda key = key: self.changeWSAD(key)
        self.funList['i'] = self.funList['w']
        self.funList['k'] = self.funList['s']
        self.funList['j'] = self.funList['a']
        self.funList['l'] = self.funList['d']

        self.funList['0'] = lambda: self.changeSpeed(100)
        for i in range(1,10):
            self.funList[str(i)] = lambda speed = i : self.changeSpeed(speed*10)

        self.initGPIO()

    def initGPIO(self):
        import RPi.GPIO as GPIO
        GPIO.setmode(GPIO.BOARD)
        for v in self.c.values():
            if v == self.c['ground']:
                continue
            print("setting channel {} as output.".format(v))
            GPIO.setup(v,GPIO.OUT)
        GPIO.output(self.c['le'], GPIO.HIGH)
        GPIO.output(self.c['re'], GPIO.HIGH)
        self.p['l'] = GPIO.PWM(self.c['le'],self.speed)
        self.p['r'] = GPIO.PWM(self.c['re'],self.speed)
        self.p['l'].start(self.speed)
        self.p['r'].start(self.speed)

    def changeSpeed(self,speed):
        self.speed = speed
        self.setSpeed()

    def setSpeed(self):
        if self.ws == 'w':
            self.leftForward()
            self.rightForward()
        elif self.ws == 's':
            self.leftBackward()
            self.rightBackward()

        leftSpeed = rightSpeed = self.speed
        if self.ad == 'a':
            leftSpeed = 0
        elif self.ad == 'd':
            rightSpeed = 0
        elif self.ad == 'p':
            pass

        self.p['l'].ChangeDutyCycle(leftSpeed)
        self.p['r'].ChangeDutyCycle(rightSpeed)
        print('Speed: ls = {}, rs = {}'.format(leftSpeed, rightSpeed))
        '''
        if self.ws == 's':
            leftSpeed *= -1
            rightSpeed *= -1
        print('Speed: ls = {}, rs = {}'.format(leftSpeed, rightSpeed))
        '''

    def leftForward(self):
        GPIO.output(self.c['l1'],GPIO.HIGH)
        GPIO.output(self.c['l2'],GPIO.LOW)
    def rightForward(self):
        GPIO.output(self.c['r1'],GPIO.HIGH)
        GPIO.output(self.c['r2'],GPIO.LOW)
    def leftBackward(self):
        GPIO.output(self.c['l1'],GPIO.LOW)
        GPIO.output(self.c['l2'],GPIO.HIGH)
    def rightBackward(self):
        GPIO.output(self.c['r1'],GPIO.LOW)
        GPIO.output(self.c['r2'],GPIO.HIGH)
    def go(self):
        GPIO.output(self.c['power'],GPIO.HIGH)
    def stop(self):
        GPIO.output(self.c['power'],GPIO.LOW)
    def pause(self):
        pass
    def quit(self):
        for v in self.p.values():
            v.stop()
        GPIO.cleanup()
    def changeWSAD(self, key):
        if key in self.tbl:
            self.ws = self.tbl[self.ws][key]
            self.ad = self.tbl[self.ad][key]
            self.setSpeed()
        else:
            print("Warning, changeWSAD with invalid value key = {}".format(key))

    def run(self):
        print('>>>')
        while True:
            cmd = self.inputLib.getch()
            cmd = chr(cmd)
            if cmd in self.funList:
                self.funList[cmd]()
            else:
                print('Warning: enter invalid cmd = {}'.format(cmd))
                self.stop()
            if cmd == 'q':
                break

remote = Remote()
remote.run()











