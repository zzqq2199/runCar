#include <stdio.h>
#include <stdlib.h>

#define STTY_US "stty raw"
#define STTY_DEF "stty -raw"

static int get_char(void)
{
    fd_set rfds;
    struct timeval tv;
    int ch = 0;
    FD_ZERO(&rfds);
    FD_SET(0,&rfds);
    tv.tv_sec=0;
    tv.tv_usec = 10;
    if (select(1,&rfds,NULL,NULL,&tv)>0)
    {
        ch = getchar();
    }
    return ch;
}

int getch(void)
{
    int ch = 0;
    system(STTY_US);
    do{
        ch = get_char();
        if (ch){
            system(STTY_DEF);
            return ch;
        }
        else{
            usleep(10);
        }
    }while(1);
    return 0;
}
